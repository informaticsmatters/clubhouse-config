#!/usr/bin/env python
#
# A simple utility to manage labels in Clubhouse.
# Refer to the project's 'config.yaml' for details
# of the configuration file format.
#
# Usage: clubhouse-config.py [--help]

from typing import Dict

import argparse
import requests
import os
import yaml

_API_TOKEN = os.environ['CLUBHOUSE_API_TOKEN']

_LABEL_API = 'https://api.clubhouse.io/api/v3/labels'

_API_HEADER = {'Content-Type': 'application/json'}
_API_PARAMS = {'token': _API_TOKEN}
_API_TIMEOUT = 4.0


def _get_labels() -> Dict[str, int]:
    """Gets the existing labels from the organisation,
    returning them as a dictionary of names against IDs.
    """

    response = requests.get(_LABEL_API,
                            headers=_API_HEADER,
                            params=_API_PARAMS,
                            timeout=_API_TIMEOUT)
    existing_labels = {}
    if response.status_code == 200:
        labels = response.json()
        for label in labels:
            existing_labels[label['name']] = label['id']
    return existing_labels


def _set_labels(config_file: str,
                existing_labels: Dict[str, int],
                archive: bool) -> None:
    """Adds labels defined in the configuration
    and optional archives those that are not.
    """

    # Labels in the configuration.
    # At the end, if archiving, labels in the existing set
    # that are not in this list are archived.
    new_labels = []

    with open(config_file) as cfg:
        config = yaml.load(cfg, Loader=yaml.FullLoader)

    for label in config['labels']:

        name = label['name']
        new_labels.append(name)

        payload = {'name': name,
                   'color': label['colour'],
                   'description': label['description']}

        if name not in existing_labels:
            # New label
            response = requests.post(_LABEL_API,
                                     headers=_API_HEADER,
                                     params=_API_PARAMS,
                                     json=payload,
                                     timeout=_API_TIMEOUT)

            if response.status_code == 201:
                print('+ {}'.format(name))
            else:
                print('? {} ERROR creating {}'.format(name, response.status_code))
        else:
            # Label already exists
            print('# {}'.format(name))

    if archive:
        for existing_label in existing_labels:
            if existing_label not in new_labels:
                payload = {'name': existing_label, 'archived': True}
                api = _LABEL_API + '/{}'.format(existing_labels[existing_label])
                response = requests.put(api,
                                        headers=_API_HEADER,
                                        params=_API_PARAMS,
                                        json=payload,
                                        timeout=_API_TIMEOUT)
                if response.status_code == 200:
                    print('- {}'.format(existing_label))
                else:
                    print('? {} ERROR updating {}'.format(existing_label,
                                                          response.status_code))


if __name__ == '__main__':

    PARSER = argparse.ArgumentParser(description='Clubhouse Configurator')
    PARSER.add_argument('config',
                        help="The YAML configuration file to use")
    PARSER.add_argument('-a', '--archive', action='store_true',
                        help="Archive existing labels if they're not in the"
                             " provided configuration file")

    ARGS = PARSER.parse_args()

    if not os.path.isfile(ARGS.config):
        PARSER.error('The config file "{}" does not exist'.format(ARGS.config))

    _set_labels(ARGS.config, _get_labels(), ARGS.archive)
