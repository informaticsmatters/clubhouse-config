# Clubhouse Config

A Python utility to configure [Clubhouse] organisations.

Given a YAML-based configuration file, `clubhouse-config.py` utilises
the Clubhouse REST [API] to: -

-   **Manage labels** for the organisation. It ensures that only labels
    defined in the config file are present in the Clubhouse organisation,
    adds new labels that aren't present and updates those that are.

## Usage
Ideally, from a Python virtual environment run the utility with: -

    $ pip install -r requirments.txt
    $ export CLUBHOUSE_API_TOKEN=<token>
    $ ./clubhouse-config.py --help

---

[clubhouse]: https://clubhouse.io
[api]: https://clubhouse.io/api/rest/v3/#Introduction
